import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './modules/database/database.module';
import { UsersModule } from './modules/users/users.module';
import { MailingModule } from './modules/mailing/mailing.module';
import { EncryptationModule } from './modules/encryptation/encryptation.module';
import { LoginModule } from './modules/login/login.module';
import { SharedFunctionsModule } from './modules/shared-functions/shared-functions.module';

@Module({
  imports: [
    DatabaseModule,
    UsersModule,
    MailingModule,
    EncryptationModule,
    LoginModule,
    SharedFunctionsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

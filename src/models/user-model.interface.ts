export interface UserModel {
    id: number,
    name: string,
    lastname: string,
    email: string,
    whatsapp: string,
    pass: string,
    lang: string,
    signupDate: string
}

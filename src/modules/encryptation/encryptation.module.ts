import { Module } from '@nestjs/common';

const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
//const key = crypto.randomBytes(32);
const hexKey = "e95702f499be475748edbac06f682b99446b7554d89ba6d0be8b9a1db03b83dc";
const hexIv = "4b614e645267556b5870327335763879";

const key = Buffer.from(hexKey, 'hex');
const iv = Buffer.from(hexIv, 'hex');

@Module({})
export class EncryptationModule {
    static encrypt(text) {
        let cipher = crypto.createCipheriv('aes-256-cbc',Buffer.from(key), iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        //return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
        return encrypted.toString('hex');
    }
        
    static decrypt(encText) {
        let iv = Buffer.from(hexIv, 'hex');
        let encryptedText = Buffer.from(encText, 'hex');
        let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    }
}

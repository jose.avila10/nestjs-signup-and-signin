import { Controller, Post, Body, BadRequestException } from '@nestjs/common';
import { LoginService } from '../service/login.service';
import { dataToLoginDTO } from '../dtos/dataToLoginDTO.dto';

@Controller('login')
export class LoginController {
    constructor(private loginServices: LoginService){
    }

    @Post()
    addUser(@Body() dataToLogin:dataToLoginDTO):any{
        if(dataToLogin.emailOrUsername === "" || dataToLogin.pass === "")
        throw new BadRequestException("Fields can't be empty");

        return this.loginServices.checkCredentials(dataToLogin);
    }
}

/*
{
    "email": "antonio.espn12@gmail.com",
    "pass": "123456"
}
*/

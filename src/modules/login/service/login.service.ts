import { Injectable, BadRequestException, UnauthorizedException } from '@nestjs/common';
import { UsersService } from 'src/modules/users/service/users.service';
import { EncryptationModule } from 'src/modules/encryptation/encryptation.module';
import { SharedFunctionsModule } from 'src/modules/shared-functions/shared-functions.module';

var jwt = require('jsonwebtoken');
const secretKey = "eSgVkYp3s6v9y$B&E)H@McQfTjWmZq4t7w!z%C*F-JaNdRgUkXp2r5u8x/A?D(G+";

@Injectable()
export class LoginService {
    constructor( private usersServices: UsersService){}

    async checkCredentials(dataToLogin:any){
        var userData = await this.usersServices.findByEmail(dataToLogin.emailOrUsername);
        if(userData === undefined){
            userData = await this.usersServices.findByUsername(dataToLogin.emailOrUsername);
            if(userData === undefined) throw new BadRequestException("Invalid email or username");
        }

        const typedPassEnc = EncryptationModule.encrypt(dataToLogin.pass);
        const userPassOnDb = userData.pass;
        
        if (typedPassEnc !== userPassOnDb) throw new BadRequestException("Invalid Password");

        const user = {
            "id": userData.id,
            "username": userData.username,
            "email": userData.email,
            "name": userData.name,
            "lastname": userData.lastname
        }

        // jwt.sign({user}, secretKey, (err, token) => {
        //     const tokenJson = {
        //         "access_token": token
        //     }
        //     console.log(tokenJson);
        // });

        const token = jwt.sign({
            data: user
        }, secretKey, { expiresIn: '10h' });

        const tokenJson = {
            "access_token": token
        }

        //401 - Invalid Login (UnauthorizedException)
        //403 - Forbidden features for logged users (ForbiddenException)\

        //const x = SharedFunctionsModule.verifyToken();
        //console.log(x);

        return tokenJson;
    }
}

/*
{
    "emailOrUsername": "antonio.espn12@gmail.com",
    "pass": "123456"
}
*/

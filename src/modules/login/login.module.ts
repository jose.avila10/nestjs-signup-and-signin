import { Module } from '@nestjs/common';
import { LoginService } from './service/login.service';
import { LoginController } from './controllers/login.controller';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [UsersModule],
  providers: [LoginService],
  controllers: [LoginController],
})
export class LoginModule {}

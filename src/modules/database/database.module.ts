import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { UsersEntity } from '../../entities/users-entity';
import { UsersModule } from '../users/users.module';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: 'localhost',
            port: 3306,
            username: 'root',
            password: '',
            database: 'money_managment',
            entities: [
                UsersEntity
            ],
            synchronize: true
        }),
        UsersModule,
    ]
})
export class DatabaseModule {
    constructor(private readonly connection: Connection) {

    }
}


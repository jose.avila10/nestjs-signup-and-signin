import { BadRequestException, Module, UnauthorizedException } from '@nestjs/common';
import e from 'express';

var jwt = require('jsonwebtoken');
const secretKey = "eSgVkYp3s6v9y$B&E)H@McQfTjWmZq4t7w!z%C*F-JaNdRgUkXp2r5u8x/A?D(G+";

@Module({})
export class SharedFunctionsModule {
    static verifyToken(token){
        try{
            var decodedToken = jwt.verify(token, secretKey);
            return decodedToken.data;
        } catch (err){
            throw new UnauthorizedException("Not loggedin");
        }
    }
}

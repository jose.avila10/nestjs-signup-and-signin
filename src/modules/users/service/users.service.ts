import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersEntity } from '../../../entities/users-entity';
//import { MailingModule } from 'src/modules/mailing/mailing.module';
import { EncryptationModule } from 'src/modules/encryptation/encryptation.module';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(UsersEntity)
        private readonly usersRP:Repository<UsersEntity>
    ) {

    }

    async addNewUser(newUserData:any){
        await this.usersRP.insert(newUserData);
        return newUserData;
    }

    async updateUser (id: number, userDataUpdated: any){
        await this.usersRP.update(id, userDataUpdated);
        return "this user with the id " + id +" has been updated and its new data is " + userDataUpdated;
    }

    async findAll(){
        return await this.usersRP.find();
    }

    async findOneUser(id: number){
        return await this.usersRP.findOne(id);
    }

    async findByEmail(email: string){
        return await this.usersRP.findOne({"email": email});
    }

    async findByUsername(username: string){
        return await this.usersRP.findOne({"username": username});
    }

    async deleteUser(id: number){
        await this.usersRP.delete(id);
        return "the user with the id " + id + " has been deleted";
    }

    async registerUser(userData:any){
        //const encPass = EncryptationModule.encrypt(userData.pass);
        //console.log(encPass);
        //const decPass = EncryptationModule.decrypt(encPass);
        //console.log(decPass);

        var checkEmailAvailability = await this.usersRP.findOne({"email": userData.email});
        if (checkEmailAvailability !== undefined) throw new BadRequestException("email already registered");

        var userAvaliability = await this.usersRP.findOne({"username": userData.username});
        if (userAvaliability !== undefined) throw new BadRequestException("username already taken");

        var passLength = userData.pass.length;
        if (passLength < 6) throw new BadRequestException("short Password (6 characters minimum)");
        const encPass = EncryptationModule.encrypt(userData.pass);
        userData.pass = encPass;

        var todayDate = this.formatDate(new Date());
        const signupDate = {
            "signupDate": todayDate
        }
        const newUserData = {...userData, ...signupDate};

        //Send email on language selected
        //MailingModule.sendWelcomeEmail(newUserData.email, newUserData.name, newUserData.lastname);

        return this.addNewUser(newUserData);
    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
    }

}

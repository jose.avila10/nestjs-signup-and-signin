export class newUserDataDTO{
    name: string;
    lastname: string;
    email: string;
    username: string;
    pass: string;
}
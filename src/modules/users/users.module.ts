import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './service/users.service';
import { UsersEntity } from 'src/entities/users-entity';
import { UsersController } from './controllers/users.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([UsersEntity])
  ],

  providers: [UsersService],

  controllers: [UsersController],

  exports: [UsersService]
})
export class UsersModule {}

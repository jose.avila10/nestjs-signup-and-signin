import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { UserModel } from 'src/models/user-model.interface';
import { newUserDataDTO } from '../dtos/newUserDataDTO.dto';
import { UsersService } from '../service/users.service';

@Controller('users')
export class UsersController {
    constructor(private usersServices:UsersService){
    }

    @Get()
    getUser():any{
        return this.usersServices.findAll();
    }

    @Get(':id')
    getOneUser(@Param() params):any{
        return this.usersServices.findOneUser(params.id);
    }

    @Post()
    addUser(@Body() newUserModel:newUserDataDTO):any{
        const lowerCaseUser = newUserModel.username.toLowerCase();
        newUserModel.username = lowerCaseUser;

        const loweCaseEmail = newUserModel.email.toLowerCase();
        newUserModel.email = loweCaseEmail;

        return this.usersServices.registerUser(newUserModel);
    }

    @Put(':id')
    updateUSer(@Body() userModel:UserModel, @Param() params):any{
        return this.usersServices.updateUser(params.id, userModel);
    }

    @Delete(':id')
    deleteUser(@Param() params):any{
        return this.usersServices.deleteUser(params.id)
    }

}

/*
{
    "name": "Jose",
    "lastname": "Avila",
    "username": "jose.avila10",
    "email": "antonio.espn@hotmail.com",
    "pass": "123456"
}
*/
